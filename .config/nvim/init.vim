" Plugins
call plug#begin()

" Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'itchyny/lightline.vim'

Plug 'scrooloose/nerdtree'

Plug 'terryma/vim-multiple-cursors'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" PHP Plugins
Plug 'SirVer/ultisnips' | Plug 'phux/vim-snippets'

call plug#end()

" Ctrl + P pesquisa arquivos
nnoremap <C-p> :Files<ENTER>
if has('nvim')
        aug fzf_setup
        au!
                au TermOpen term://*FZF tnoremap <silent> <buffer><nowait> <esc> <c-c>
  aug END
end


" NERDTree
nnoremap <silent> <C-B> :NERDTreeToggle<CR>
autocmd VimEnter * NERDTree | wincmd p
nmap <leader>nf :NERDTreeFind<CR>
" Ignorar pastas
let g:NERDTreeIgnore = ['^node_modules$']
" sync open file with NERDTree
" Check if NERDTree is open or active
function! IsNERDTreeOpen()
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

" Call NERDTreeFind iff NERDTree is active, current window contains a modifiable
" file, and we're not in vimdiff
function! SyncTree()
  if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
    NERDTreeFind
    wincmd p
  endif
endfunction
" sync open file with NERDTree
" " Check if NERDTree is open or active
function! IsNERDTreeOpen()
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

" Highlight currently open buffer in NERDTree
autocmd BufEnter * call SyncTree()


"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

set relativenumber
set nu

set smarttab
set cindent
set tabstop=2
set shiftwidth=2
set expandtab
set noshowmode

syntax on
" colorscheme dracula


" PHP

let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"

" PHP7
let g:ultisnips_php_scalar_types = 1